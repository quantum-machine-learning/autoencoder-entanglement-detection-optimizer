### Quantum Machine Learning

In quantum information theory, a mixed state of the composite system is described by a density matrix acting on the composite Hilbert space (i.e the tensor product of the sub-systems Hilbert spaces)[1].
In these composite systems, a physical phenomenon can occur, called entanglement, such that the quantum state of each subsystem cannot be described independent of the state of the other subsystems, and measurement of physical properties performed on entangled subsystems are found to be correlated. Not all the composite systems have this property; a state without quantum entanglement is called a separable quantum state.

Given a density matrix for a composite system, the task to find whether or not the state is entangled in general is considered to be a difficult task, although in some special cases there exists a simple analytic method[2]. This problem is sometimes called the separability problem in quantum information theory and it's been shown to be NP-hard[3]. A separability criterion or an entanglement witness is a necessary condition a density matrix must satisfy to be separable or entangled, respectively. But it's only a necessary condition (and not sufficient) so that if the criterion or witness does not hold, the test is inconclusive.

In cases which the Hilbert space dimension is (2,2) or (2,3) (or in general d_a * d_b <= 6 ) , the Peres-Horodecki criterion (also known as PPT criterion) is a necessary and sufficient condition for separability[2]. In higher dimensions, the test is inconclusive. The criterion states that in bipartite states if a state is separable, its partial transpose has non-negative eigenvalues. In other words, if the partial transpose has a negative eigenvalue, the state is guaranteed to be entangled. The result is independent of the party that was transposed. In a more recent work [4], it has been proven that for a Hilbert space where the dimensions are (2,2) the PPT criterion will necessary and sufficiently reduce to the sign of the determinant of the given density matrice, which means that if the determinant of the given density matrice is negative the state is entangled.

In a two-qubit state, 15 features from the tensor product of Pauli matrices and identity operator, can fully describe a density matrix (neglecting the trivial 1 resulted from the identity operators). The very first step of our project starts with the task that given these 15 features, machine should learn to identify whether a density matrix is separable or not. It is expected that machine achieves a 1.0 score with supervised learning for this case since the PPT criterion can be found from the determinant of the partially transposed matrix, which is in turn a 4th-degree polynomial of these 15 features. So a linear regression with a 4th-degree polynomial feature should work fine but one of the goals of this work is to reduce the number of features (measurements) to determine whether a state is entangled or not. 

In this project, we propose to use autoencoder neural networks to ﬁnd semi-optimal measurements for entanglement detection and construct new entanglement witnesses that can detect entangled states with incomplete data, e.g. with as few as three measurements. 
   
The set of semi-optimal measurements found with the traditional autoencoder neural network was a complex function of the 15 original features, the number of measurements in these sets is reduced but implementing these sets of measurements in practice might be very challenging due to its complexity. So we propose an altered autoencoder neural network where the 'encoding' part is reduced and the network is forced to select the semi-optimal sets by a linear combination of the 15 original features. Since the semi-optimal measurements in this step where a linear combination of the 15 features, their implementation in practice will be easier in comparison to the sets found with the traditional autoencoder neural network.

The quantum states used in laboratories might have a certain symmetry. In the next step, we propose an autoencoder neural network structure specifically for the cylindrical symmetric two-qubit states, the results indicate that the accuracy is higher in comparison to the network which was trained with no specific symmetric data.

The noise robustness of these methods was also checked and the accuracy of these models stays above 0.8, with the added Gaussian noise(standard deviation of the added noise were below 0.5).

Finally, we can employ these machine learning methods to higher dimensions to see whether it is possible to find a less complex separability criterion or entanglement witness with high certainty.

-----
[1] Isaac Chuang and Michael Nielsen, Quantum Computation and Quantum Information, second ed. , 2010

[2] M. Horodecki, P. Horodecki, R. Horodecki, Separability of mixed states: necessary and sufficient conditions, Phys. Lett. A 223, 1 ,1996.

[3] Sevag Gharibian, Strong NP-Hardness of the Quantum Separability Problem, Quantum Information and Computation, Vol. 10, No. 3&4, pp. 343-360, 2010. arXiv:0810.4507.

[4]  R. Augusiak, M. Demianowicz, and P. Horodecki, Physical Review A - Atomic, Molecular, and Optical Physics 77, 1 (2008). 
